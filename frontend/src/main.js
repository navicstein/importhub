import "@babel/polyfill";
import Vue from "vue";
import "./plugins/vuetify";
import "./plugins/axios";
import "./plugins/sails";
import "./plugins/socket";
import "./plugins/currency";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import "@/assets/css/overide.css";
import "lodash";

// Vue Head
import VueHead from "vue-head";

import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
Vue.use(VueHead);
// Time & Date Filtering
Vue.use(require("vue-moment"));

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
