import Vue from "vue";
var url =
  process.env.NODE_ENV == "development"
    ? "http://localhost:1337"
    : "http://178.128.207.83";
var _sails = {
  BASE_URL: url,
  API_URL: url + "/api/v1"
};
const Sails = {
  install(Vue, options) {
    Vue.mixin({
      beforeMount() {
        Vue.prototype.sails = _sails;
        window.sails = _sails;
      }
    });
  }
};
Vue.use(Sails);
export default Sails;
