import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: "vuex",
  storage: window.localStorage
});
// var rand = myArray[Math.floor(Math.random() * myArray.length)];
export default new Vuex.Store({
  state: {
    // cart items
    cart: [],
    // user
    user: {},
    userProducts: {},
    products: [],

    // When ever a user vists a product..
    // .. save the id here so as to use it for `based on what you've viewed`
    viewedProductCategory: "",
    // loaders
    loading: true,
    loadingError: false,
    oops: false
  },
  mutations: {
    /**
     * @description Sets and Sync's a user
     * @param {*} state
     * @param {*} heystack
     */
    setUser(state, heystack) {
      state.user = heystack;
    },

    /**
     * @description Set's a vendors profile products or category products
     * @param {*} state
     * @param {*} heystack
     */
    setProducts(state, heystack) {
      state.products = heystack;
    },
    /**
     * @description sets User Products for profile
     * @param {*} state
     * @param {*} heystack
     */
    setUserProducts(state, heystack) {
      state.userProducts = heystack;
    },
    /**
     * @description Saves the latest product category for use in some pages
     * @param {*} state
     * @param {*} heystack
     * @example "Computers"
     */
    saveProductCategory(state, heystack) {
      state.viewedProductCategory = heystack;
    },
    // for cart
    addToCart(state, heystack) {
      var previousItem = state.cart.find(x => x.id == heystack.id);
      if (typeof previousItem === "undefined") {
        heystack.quantity = 1;
        state.cart.push(heystack);
        // console.log("added Item", heystack);
      } else {
        previousItem.quantity++;
        // console.log("Previous", previousItem.quantity);
      }
    },
    /**
     * @description Increment cart quantity
     * @param {*} state
     * @param {*} heystack
     */
    incrementCartQty(state, heystack) {
      var previousItem = state.cart.find(x => x.id == heystack.id);
      previousItem.quantity++;
    },
    /**
     * @description Decrement cart quantity
     * @param {*} state
     * @param {*} heystack
     */
    decrementCartQty(state, heystack) {
      var previousItem = state.cart.find(x => x.id == heystack.id);
      previousItem.quantity > previousItem.moq ? previousItem.quantity-- : null;
    },
    /**
     * @description Remove item from cart
     * @param {*} state
     * @param {*} heystack
     */
    removeFromCart: (state, heystack) => {
      const cart = state.cart.findIndex(t => t.id == heystack.id);
      state.cart.splice(cart, 1);
    }
  },
  actions: {},
  plugins: [vuexLocalStorage.plugin]
});
