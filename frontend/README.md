# Ecommerce website

> This is a default ecommerce full app

# Todo

- removing of <hr> tomorrow?

## What's included

- Vue 3 [frontend]
- Sailsjs [backend]
- Vuetify [material toolkit]

# What's next?

- Add products page
- Selection page
- Dashboards
- Categories page
- Authentication
