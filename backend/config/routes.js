/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  /**SIGNUP */
  "POST /api/v1/entrance/signin": "entrance/login",
  "POST /api/v1/entrance/signup": "entrance/signup",

  /** SYNC USER */
  "POST /api/v1/sync-user": { action: "syncs/sync-user" },
  "DELETE /api/v1/unsync-user": { action: "syncs/unsync-user" },

  /** === PRODUCT === */
  "PUT /api/v1/product": { action: "product/create" },
  "PATCH /api/v1/product": { action: "product/update" },
  "POST /api/v1/product/upload-photo": { action: "product/upload-photo" },
  "DELETE /api/v1/product/delete-photo": { action: "product/delete-photo" },

  /** PHOTO */
  "GET /api/v1/stream-photo/:id": { action: "product/download-photo" }, // stream photo
  "GET /api/v1/product/:id/photo": { action: "product/download-photo" }, // stream photo
  "GET /api/v1/product/:id": { action: "product/find" }, // find `one` product

  "DELETE /api/v1/product": { action: "product/delete" },
  "GET /api/v1/products": { action: "product/all" },
  "GET /api/v1/product/search-toggle": { action: "product/search-toggle" },

  /** SITE SETTINGS */
  "POST /api/v1/site/upload-images": { action: "site/upload-images" },
  "GET /api/v1/site/:type/get-images": { action: "site/get-images" },
  "GET /api/v1/pipe-image/:id": { action: "site/pipe-image" },
  "DELETE /api/v1/site/delete-image": { action: "site/delete-image" },
  "PATCH /api/v1/site/update-image-link-or-info": {
    action: "site/update-image-link-or-info"
  },

  "PATCH /api/v1/site/conversion-value": {
    action: "site/conversion-value"
  },

  /** CONTACT FORMS */
  "POST /api/v1/contact/contact-us": { action: "contact/contact-us" },

  // related
  "POST /api/v1/product/related": { action: "product/related" },
  "POST /api/v1/product/related-vendor-products": {
    action: "product/related-vendor-products"
  },

  /** Admin actions */
  "GET /api/v1/admin/users": { action: "admin/users/get" },
  "DELETE /api/v1/admin/user": { action: "admin/users/delete" },
  "PATCH /api/v1/admin/user": { action: "admin/users/patch" },
  "GET /api/v1/admin/orders": { action: "admin/orders" },

  /** CART */
  "POST /api/v1/cart/overview": { action: "cart/overview" },
  "POST /api/v1/cart": { action: "cart/add-to-cart" },
  "POST /api/v1/cart/save-order-info": { action: "cart/save-order-info" },

  /** === Categories === */
  "GET /api/v1/category": { action: "category/find" },
  "GET /api/v1/category/:category": { action: "category/find-products" },
  "GET /api/v1/category/get-all": {
    action: "category/get-all"
  },
  "POST /api/v1/category/create-category": {
    action: "category/create-category"
  },
  "GET /api/v1/category/find-products": { action: "category/find-products" },
  "DELETE /api/v1/category": { action: "category/delete-category" },

  /** === VENDOR === */
  "POST /api/v1/vendor/profile": { action: "vendor/profile" }, // remove after update
  "POST /api/v1/vendor": { action: "vendor/profile" },
  "PATCH /api/v1/vendor": { action: "vendor/update-store" },
  "POST /api/v1/vendor/upload-photo": { action: "vendor/upload-photo" },

  /** === WALET === */
  "POST /api/v1/wallet/wallet": { action: "wallet/wallet" },
  "PUT /api/v1/wallet": { action: "wallet/fund" },
  "PUT /api/v1/wallet/add-card": { action: "wallet/add-card" },

  "GET /api/v1/user": { action: "user/profile" },

  /** === General Piping Of Images | Image === */
  "GET /api/v1/image/:type/:id": { action: "image/pipe-image" },
  "GET /image/:type/:id": { action: "image/pipe-image" },

  /** === SEARCH === */
  "POST /api/v1/search": { action: "search/search" },

  /** === MESSAGING === */
  "POST /api/v1/messaging": {
    action: "messaging/create-message"
  },
  "GET /api/v1/messaging": { action: "messaging/get-message" },
  "DELETE /api/v1/messaging": { action: "messaging/delete-message" }
};
