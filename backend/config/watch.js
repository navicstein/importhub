module.exports.watch = {
  active: true,
  usePolling: false,
  dirs: [
    "api/",
    "config/"
  ],
    ignored: [
    "node_modules",
    // Ignore all files with .ts extension
    "**.ts"
  ]
};
