module.exports = {
  friendlyName: "Get message",

  description: "",

  inputs: {
    from: { type: "string", required: true },
    to: { type: "string", required: true }
  },

  exits: {},

  fn: async function(inputs) {
    var messages = await Messaging.find().where({
      from: inputs.from,
      to: inputs.to
    });
    // All done.
    return messages;
  }
};
