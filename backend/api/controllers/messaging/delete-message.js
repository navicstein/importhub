module.exports = {
  friendlyName: "Delete message",

  description: "",

  inputs: {
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var rmMsg = await Messaging.destroy({ id: inputs.id });
    // All done.
    return rmMsg;
  }
};
