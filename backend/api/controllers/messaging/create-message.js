module.exports = {
  friendlyName: "Create message",

  description: "",

  inputs: {
    from: { type: "string", required: true },
    to: { type: "string", required: true },
    message: { type: "string", required: true }
  },

  exits: {},

  fn: async function(inputs) {
    await Messaging.destroy({});
    var newMessage = await Messaging.create({
      from: inputs.from,
      to: inputs.to,
      message: inputs.message
    }).fetch();
    sails.log(newMessage);
    // All done.
    return newMessage;
  }
};
