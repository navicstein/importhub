module.exports = {
  friendlyName: "Sync user",

  description:
    "This actions synchronizes the user/vendor/admin with the frontend ",

  inputs: {
    /**No inputs */
  },

  exits: {
    noUser: {
      statusCode: 407,
      description: "no user found"
    }
  },

  fn: async function(inputs) {
    // check for siged in user
    if (this.req.me) {
      // sure there's a signed in user,
      // .. find the user to populate his details
      var user = await User.findOne()
        .where({ id: this.req.me.id })
        .populateAll();
      // .. look up his products if he's a vendor
      var products = await Product.find()
        .where({ vendor: user.id })
        .populateAll();
      return { user, products };
    } else {
      throw "noUser";
    }
    // All done.
  }
};
