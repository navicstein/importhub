module.exports = {
  friendlyName: "Unsync user",

  description: "",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    delete this.req.session.userId;
    this.res.clearCookie();
    // All done.
    return;
  }
};
