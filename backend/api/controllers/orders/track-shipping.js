module.exports = {
  friendlyName: "Track shipping",

  description: "",

  inputs: {
    trackingNumber: {
      type: "string",
      required: true
    }
  },

  exits: {
    notfound: {
      statusCode: 404,
      reponseType: "notfound",
      description: "Tracking Number not found"
    }
  },

  fn: async function(inputs) {
    var trackingNumber = await await Order.findOne()
      .where({
        trackingNumber: inputs.trackingNumber
      })
      .populateAll();
    if (!trackingNumber) throw "notfound";
    // All done.
    return trackingNumber;
  }
};
