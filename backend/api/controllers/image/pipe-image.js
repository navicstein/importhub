module.exports = {
  friendlyName: "Pipe IMAGE",

  description: "General Piping of image file (returning a stream).",

  inputs: {
    id: {
      description: "The id of the item whose photo we're Pipeing.",
      type: "string",
      required: true
    }
  },

  exits: {
    success: {
      outputDescription: "The streaming bytes of the specified thing's photo.",
      outputType: "ref"
    },

    forbidden: { responseType: "forbidden" },
    notFound: { responseType: "notFound" }
  },

  fn: async function(inputs) {
    var photo;

    // lookup photo from SiteImage db
    photo = await SiteImage.findOne().where({
      id: inputs.id
    });
    // if notfound ?
    if (!photo || _.isEmpty(photo)) {
      // lookup from another table
      photo = await Image.findOne().where({ id: inputs.id });
    } else return;
    // set the type of MIME
    this.res.type(photo.imageUploadMime);
    // start image piping
    var Pipeing = await sails.startDownload(photo.imageUploadFd);
    return Pipeing; // All done.
  }
};
