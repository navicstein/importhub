module.exports = {
  friendlyName: "Add Card",

  description: "Add Card To wallet.",

  inputs: {
    cardNumber: {
      type: "string",
      example: "0000 0000 0000 0000",
      required: true
    },
    cardExpDate: {
      type: "string",
      required: true,
      example: "43/93"
    },

    cVv: { type: "string", required: true, example: "123" }
  },

  exits: {
    cardAlreadyExist: {
      statusCode: 409,
      description: "credit card already exist"
    }
  },

  fn: async function(inputs) {
    // await Wallet.destroy({});
    var walletExist = await Wallet.findOne().where({
      cardNumber: inputs.cardNumber
    });
    if (walletExist) {
      throw "cardAlreadyExist";
    }

    var newCreditCard = await Wallet.create({
      cardNumber: inputs.cardNumber,
      cardExpDate: inputs.cardExpDate,
      cVv: inputs.cVv,
      user: this.req.me.id
    }).fetch();
    // All done.
    return newCreditCard;
  }
};
