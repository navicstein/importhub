module.exports = {
  friendlyName: "Fund",

  description: "Fund wallet.",

  inputs: {
    card: {
      type: "string",
      required: true,
      description: "The id of the card to choose"
    },
    amount: {
      type: "string",
      required: false,
      description: "the amount he wants to add to his wallet"
    }
  },

  exits: {},

  fn: async function(inputs) {
    // get the props of the card first ..
    // ..
    var card = await Wallet.findOne().where({
      id: inputs.card,
      user: this.req.me.id
    });

    // We've gotten the card, lets send a request to paystack
    // ..
    // .. lets debit the user now and update his wallet

    // var someRandomStuffFromPaystack

    // .. now fund the wallet
    var amountToUpdate = await Wallet.update({
      id: card.id,
      user: this.req.me.id
    }).set({
      amount: inputs.amount
    });

    amountToUpdate = amountToUpdate[0];

    return amountToUpdate;
  }
};
