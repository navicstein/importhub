module.exports = {
  friendlyName: "Wallet",

  description: "Wallet wallet.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var wallet = await Wallet.find().where({ user: this.req.me.id });
    // All done.
    return wallet;
  }
};
