module.exports = {
  friendlyName: "Add to cart",

  description: "",

  inputs: {
    cartItem: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    this.req.session.cartItem = inputs.cartItem;
    var cartItem = this.req.session.cartItem;
    var product = await Product.findOne().where({ id: inputs.cartItem });

    // product.cartQuantity = 1;
    product.cartQuantity += 1;
    sails.log.warn(cartItem, product);
    // All done.
    return;
  }
};
