module.exports = {
  friendlyName: "Save order info",

  description: "",

  inputs: {
    products: {
      type: "ref",
      required: true
    },
    country: {
      type: "string",
      required: true
    },
    state: {
      type: "string",
      required: true
    },
    city: {
      type: "string",
      required: true
    },
    phoneNumber: {
      type: "string",
      required: true
    },
    fullName: {
      type: "string",
      required: true
    },
    address: {
      type: "string",
      required: true
    },
    emailAddress: {
      type: "string",
      required: true
    }
  },
  exits: {
    duplicateOrder: {
      statusCode: 409,
      description: "Order has already been placed"
    }
  },

  fn: async function(inputs) {
    // helper fns
    function generateTrackingNumber() {
      let text = "";
      let possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (let i = 0; i < 10; ++i) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    // look up order first
    // var previousOrder = await Order.findOne().where({
    //   user: this.req.me.id
    // });
    // if (previousOrder) throw "duplicateOrder";
    // .. else save a new order
    var newOrder = await Order.create({
      user: this.req.me.id,
      emailAddress: inputs.emailAddress,
      country: inputs.country,
      state: inputs.state,
      city: inputs.city,
      phoneNumber: inputs.phoneNumber,
      fullName: inputs.fullName,
      address: inputs.address,
      products: inputs.products,
      trackingNumber: generateTrackingNumber()
    }).fetch();

    // All done.
    return newOrder;
  }
};
