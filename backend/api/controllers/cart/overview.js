module.exports = {
  friendlyName: "Overview",

  description: "Overview cart.",

  inputs: {
    ids: {
      type: ["string"],
      required: true,
      description: "The list of ids of cart to display"
    }
  },

  exits: {},

  fn: async function(inputs) {
    var cartItems = await Product.find()
      .where({
        id: { in: inputs.ids }
      })
      .populate("photos")
      .populate("vendor");
    // All done.
    return cartItems;
  }
};
