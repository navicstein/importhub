module.exports = {
  friendlyName: "Delete",

  description: "Delete users.",

  inputs: {
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function({ id }) {
    // rm products first
    await Product.destroy({ vendor: id });
    await Image.destroy({ vendor: id });
    await Wallet.destroy({ vendor: id });
    await Review.destroy({ vendor: id });
    await Order.destroy({ vendor: id });
    await Messaging.destroy({ vendor: id });
    await Cart.destroy({ vendor: id });
    await User.destroy({ id: id });
    // All done.
    return;
  }
};
