module.exports = {
  friendlyName: "Get",

  description: "Get users.",

  inputs: {
    type: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var users;
    /** === TODO === */
    // === remember to add .limit() && .skip() for pagination ===
    if (inputs.type == "vendors") {
      users = await User.find()
        .where({
          isVendor: !0
        })
        .populateAll();
      return users;
    } else {
      users = await User.find()
        .where({
          isVendor: !1
        })
        .populateAll();
      return users;
    }

    // All done.
  }
};
