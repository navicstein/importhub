module.exports = {
  triendlyrext: "Create category from the admin panel",

  description: "",

  inputs: {
    parent: {
      type: "string",
      required: true
    },
    category: {
      type: "string",
      required: true
    },
    subCategory: {
      type: "string",
      required: false
    }
  },

  exits: {
    categoryOrParentAlreadyExist: {
      statusCode: 409,
      description: "The provided category of parent is already in use."
    }
  },

  fn: async function(inputs) {
    // await Category.destroy({});
    var parent = inputs.subCategory
      ? inputs.parent + ", " + inputs.subCategory
      : inputs.parent;
    var category = await Category.create({
      parent: parent,
      text: inputs.category,
      value: _.kebabCase(inputs.category)
    }).fetch();
    return category;
  }
};
