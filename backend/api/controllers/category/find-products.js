module.exports = {
  friendlyName: "Find products",

  description: "",

  inputs: {
    category: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var category = inputs.category;
    sails.log(category);
    sails.log(_.kebabCase(category));
    var products = await Product.find()
      .where({
        // subCategory: _.kebabCase(category)
        or: [
          {
            category: { contains: category }
          },
          {
            subCategory: { contains: category }
          }
        ]
      })
      .populateAll();

    // All done.
    return products;
  }
};
