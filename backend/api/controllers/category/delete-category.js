module.exports = {
  friendlyName: "Delete category",

  description: "",

  inputs: {
    id: { type: "string", required: true }
  },

  exits: {},

  fn: async function({ id }) {
    await Category.destroy({ id: id });
    // All done.
    return;
  }
};
