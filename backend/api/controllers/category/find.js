module.exports = {
  friendlyName: "Find",

  description: "Find tag.",

  inputs: {
    tag: {
      type: "string",
      required: false
    },
    parent: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    // var products = await Product.find()
    //   .where({
    //     or: [
    //       {
    //         category: { contains: category }
    //       },
    //       {
    //         subCategory: { contains: category }
    //       }
    //     ]
    //   })
    //   .populateAll();

    var category = await Category.find().where({
      parent: { contains: inputs.parent }
    });
    // All done.
    return category;
  }
};
