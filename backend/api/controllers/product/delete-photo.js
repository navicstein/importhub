module.exports = {
  friendlyName: "Delete photo",

  description: "",

  inputs: {
    id: { type: "string", required: true }
  },

  exits: {},

  fn: async function({ id }) {
    var pix = await ProductPhotos.findOne({ id: id });
    try {
      await sails.rm(pix.imageUploadFd);
      await ProductPhotos.destroy({ id: pix.id });
    } catch (e) {
      sails.log.error(e);
    }
    // All done.
    return;
  }
};
