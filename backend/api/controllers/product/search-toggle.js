module.exports = {
  friendlyName: "Search toggle",

  description: "",

  inputs: {
    price: { type: "string", required: false },
    brand: { type: ["string"], required: false },
    category: { type: ["string"], required: false }
  },

  exits: {},

  fn: async function(inputs) {
    // var price = inputs.price.replace(/₦/gi, "");

    // // minimum and max price
    // price = price.split("-");
    // var minPrice = price[0],
    //   maxPrice = price[1];

    // async function runQuery(attribs) {
    //   return products;
    // }

    // var query;
    // if (inputs.brand) {
    //   return (query = await runQuery({
    //     brand: { in: inputs.brand.split(",") }
    //   }));
    // } else if (inputs.price) {
    //   return (query = await runQuery({
    //     price: { "<=": minPrice }
    //   }));
    // }

    var brand =
      typeof inputs.brand === "object" ? inputs.brand : Array(inputs.brand); // Array
    var price = Number(inputs.price);
    sails.log.warn("price", price);
    sails.log.warn("brand", brand);
    sails.log.warn("category", inputs.category || [""]);

    var products = await Product.find()
      .where({
        or: [
          {
            brand: {
              in: brand
            }
          },
          {
            category: {
              in: inputs.category || [""]
            }
          },
          {
            subCategory: {
              in: inputs.category || [""]
            }
          }
        ]
      })
      .populateAll();

    // sails.log.warn(products);

    // All done.
    return products;
  }
};
