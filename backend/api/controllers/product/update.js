module.exports = {
  friendlyName: "Update Product",

  description: "Update product.",

  inputs: {
    id: {
      type: "string",
      required: true
    },

    name: {
      type: "string",
      required: true
    },
    subCategory: {
      type: "string",
      required: true
    },
    moq: {
      type: "string",
      required: true
    },
    description: {
      type: "string",
      required: true
    },
    specifications: {
      type: "json",
      required: true
    },
    availablity: {
      type: "boolean",
      required: false
    },
    // category would be dynamic
    category: {
      type: "string",
      required: true
    },
    price: {
      type: "number",
      required: true
    },
    weigth: {
      type: "string",
      required: false
    },
    stockQuantity: {
      type: "string",
      required: false
    },
    marketPrice: {
      type: "string",
      required: false
    },
    returnPolicy: {
      type: "string",
      required: false
    },
    requiresShipping: {
      type: "boolean",
      required: false
    },
    arrivalDate: {
      type: "string",
      required: false
    },
    sku: {
      type: "string",
      required: false
    }
  },

  exits: {},

  fn: async function(inputs) {
    var newProduct = await Product.update({ id: inputs.id })
      .set(
        Object.assign({
          name: inputs.name,
          slug: _.kebabCase(inputs.name),
          description: inputs.description,
          category: inputs.category,
          specifications: inputs.specifications,
          availablity: inputs.availablity,
          price: Number(inputs.price),
          vendor: this.req.me.id,
          moq: inputs.moq,
          subCategory: inputs.subCategory,
          weigth: inputs.weigth,
          marketPrice: inputs.marketPrice,
          requiresShipping: inputs.requiresShipping,
          arrivalDate: inputs.arrivalDate,
          stockQuantity: inputs.stockQuantity,
          returnPolicy: inputs.returnPolicy,
          sku: inputs.sku
        })
      )
      .fetch();

    this.req.session.productId = newProduct.id;

    // All done.
    return newProduct[0];
  }
};
