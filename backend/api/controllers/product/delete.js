module.exports = {
  friendlyName: "Delete",

  description: "Delete product.",

  inputs: {
    id: { type: "string", required: true }
  },

  exits: {},

  fn: async function(inputs) {
    const fs = require("fs");
    try {
      var product = await Product.findOne().where({ id: inputs.id }),
        photos = await ProductPhotos.find().where({ product: product.id });
      photos.forEach(photo => fs.unlinkSync(photo.imageUploadFd));
      await ProductPhotos.destroy({ id: product.id });
      await Product.destroy({ id: inputs.id });
    } catch (e) {
      sails.log.error(e);
    }
    // All done.
    return;
  }
};
