module.exports = {
  friendlyName: "Create",

  description: "Create product.",

  inputs: {
    name: {
      type: "string",
      required: true
    },
    subCategory: {
      type: "string",
      required: true
    },
    moq: {
      type: "string",
      required: true
    },
    description: {
      type: "string",
      required: true
    },
    specifications: {
      type: "json",
      required: true
    },
    availablity: {
      type: "boolean",
      required: false
    },
    // category would be dynamic
    category: {
      type: "string",
      required: true
    },
    price: {
      type: "number",
      required: true
    },
    weigth: {
      type: "string",
      required: false
    },
    stockQuantity: {
      type: "string",
      required: false
    },
    marketPrice: {
      type: "string",
      required: false
    },
    returnPolicy: {
      type: "string",
      required: false
    },
    requiresShipping: {
      type: "boolean",
      required: false
    },
    arrivalDate: {
      type: "string",
      required: false
    },
    sku: {
      type: "string",
      required: false
    },
    seo: {
      type: "json",
      required: true
    }
  },

  exits: {
    productAlreadyExist: {
      statusCode: 409,
      description: "The product  already exist"
    }
  },

  fn: async function(inputs) {
    var isProductExisting = await Product.findOne({ name: inputs.name });
    if (isProductExisting) throw "productAlreadyExist";
    /**
     * @description TODO
     * @emits check if `sku` is available before attempting to create, or intercept sku
     */
    var newProduct = await Product.create(
      Object.assign({
        name: inputs.name,
        slug: _.kebabCase(inputs.name),
        description: inputs.description,
        category: inputs.category,
        specifications: inputs.specifications,
        availablity: inputs.availablity,
        price: Number(inputs.price),
        vendor: this.req.me.id,
        moq: inputs.moq,
        subCategory: inputs.subCategory,
        weigth: inputs.weigth,
        marketPrice: inputs.marketPrice,
        requiresShipping: inputs.requiresShipping,
        arrivalDate: inputs.arrivalDate,
        stockQuantity: inputs.stockQuantity,
        returnPolicy: inputs.returnPolicy,
        sku: inputs.sku,
        seo: inputs.seo
      })
    )
      .intercept("E_UNIQUE", "productAlreadyExist")
      .fetch();

    this.req.session.productId = newProduct.id;

    // All done.
    return newProduct;
  }
};
