module.exports = {
  friendlyName: "Find",

  description: "Find product.",

  inputs: {
    id: {
      type: "string",
      required: true,
      description: "The ID or shortName of the product"
    },
    category: {
      type: "string",
      required: false,
      description: "The category of the product"
    }
  },

  exits: {
    notFound: {
      responseType: "notfound",
      description: "The product is not found"
    }
  },

  fn: async function(inputs) {
    var product = await Product.findOne()
      .where({
        or: [{ slug: inputs.id }, { id: inputs.id }]
      })
      .populateAll();

    if (!product) throw "notFound";
    this.req.session.productId = product.id;

    // if (inputs.category) {
    //   var product = await Product.find().where({
    //     or: [{ id: inputs.id }, { shortName: inputs.id }]
    //   });

    // }
    // All done.
    return product;
  }
};
