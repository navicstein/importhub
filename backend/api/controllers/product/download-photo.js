module.exports = {
  friendlyName: "Download photo",

  description: "Download photo file (returning a stream).",

  inputs: {
    id: {
      description: "The id of the item whose photo we're downloading.",
      type: "string",
      required: true
    }
  },

  exits: {
    success: {
      outputDescription: "The streaming bytes of the specified thing's photo.",
      outputType: "ref"
    },

    forbidden: { responseType: "forbidden" },

    notFound: { responseType: "notFound" }
  },

  fn: async function(inputs) {
    var photo = await ProductPhotos.findOne().where({
      id: inputs.id
    });

    if (!photo || _.isEmpty(photo)) {
      throw "notFound";
    }

    this.res.type(photo.imageUploadMime);

    var downloading = await sails.startDownload(photo.imageUploadFd);

    return downloading; // All done.
  }
};
