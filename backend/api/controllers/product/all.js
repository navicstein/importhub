module.exports = {
  friendlyName: "All Product",

  description: "Get All products.",

  inputs: {
    category: {
      type: "string",
      required: false,
      description: "The category of the products"
    },
    limit: {
      type: "string",
      required: false,
      description: "The limit of all products"
    },
    skip: {
      type: "string",
      required: false,
      description: "The limit of all products"
    }
  },

  exits: {
    notFound: {
      responseType: "notfound",
      description: "The product is not found"
    }
  },

  fn: async function(inputs) {
    // sort by category
    if (inputs.category) {
      return await Product.find()
        .where({
          or: [{ subCategory: inputs.category }, { category: inputs.category }]
        })
        .populateAll()
        .limit(inputs.limit)
        .skip(inputs.skip);
    }

    var product = await Product.find()
      .populateAll()
      .limit(inputs.limit)
      .skip(inputs.skip);
    return product;
  }
};
