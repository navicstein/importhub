module.exports = {
  friendlyName: "Get user",

  description: "",

  inputs: {},

  exits: {
    noUser: {
      statusCode: 407,
      description: "no user found"
    }
  },

  fn: async function(inputs) {
    // All done.
    // sails.log(this.req);

    // sails.log.warn(this.req.me);
    if (this.req.me) {
      return { user: this.req.me };
    } else {
      throw "noUser";
    }
  }
};
