module.exports = {
  friendlyName: "Upload vendor photo and banner",

  description: "",

  files: ["photo"],
  inputs: {
    photo: {
      description: "Upstream for an incoming file upload.",
      type: "ref",
      required: true
    },
    type: {
      description: "Type of upload, banner | logo",
      type: "string",
      required: true
    }
  },

  exits: {
    tooBig: {
      description: "The file is too big.",
      responseType: "badRequest"
    }
  },

  fn: async function({ photo, type }) {
    var util = require("util");

    // delete previous image[s] before uploading
    var user = this.req.me;
    try {
      _.map(user.storeImages, async image => {
        if (image.type == type) {
          await sails.rm(image.imageUploadFd);
          await Image.destroy({ vendor: user.id, type: image.type });
        }
      });
    } catch (e) {
      sails.log.error(e);
    }

    var photo = await sails
      .uploadOne(photo, {
        maxBytes: 3000000,
        saveAs: this.req.me.id + "-" + type + ".jpg"
      })
      .intercept("E_EXCEEDS_UPLOAD_LIMIT", "tooBig")
      .intercept(
        err => new Error("The photo upload failed: " + util.inspect(err))
      );
    var image = await Image.create({
      vendor: this.req.me.id,
      size: photo.size,
      imageUploadFd: photo.fd,
      imageUploadMime: photo.type,
      type: type
    }).fetch();
    return image;
  }
};
