module.exports = {
  friendlyName: "Profile",

  description: "Profile vendor.",

  inputs: {
    url: {
      type: "string",
      required: true
    },
    limit: { type: "string", required: false }
  },

  exits: {
    notfound: {
      responseType: "notfound",
      description: "vendor not found"
    }
  },

  fn: async function(inputs) {
    // get vendor
    var vendor = await User.findOne()
      .where({
        nameSlug: inputs.url,
        isVendor: true
      })
      .populateAll();

    if (!vendor) throw "notfound";

    var products = await Product.find()
      .where({
        vendor: vendor.id
      })
      .limit(inputs.limit || 3)
      .populateAll();
    // All done.
    return { vendor, products };
  }
};
