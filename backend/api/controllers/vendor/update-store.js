module.exports = {
  friendlyName: "Update store",

  description: "",

  inputs: {
    description: {
      type: "string",
      required: true
    },
    logo: {
      type: "string",
      required: false
    },
    banner: {
      type: "string",
      required: false
    },
    name: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var store = await User.update({ id: this.req.me.id })
      .set({
        fullName: inputs.name.trim(),
        storeDescription: inputs.description.trim()
      })
      .fetch();
    // All done.
    return store;
  }
};
