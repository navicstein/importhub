module.exports = {
  friendlyName: "Pipe photo",

  description: "Pipe photo file (returning a stream).",

  inputs: {
    id: {
      description: "The id of the item whose photo we're Pipeing.",
      type: "string",
      required: true
    }
  },

  exits: {
    success: {
      outputDescription: "The streaming bytes of the specified thing's photo.",
      outputType: "ref"
    },

    forbidden: { responseType: "forbidden" },
    notFound: { responseType: "notFound" }
  },

  fn: async function(inputs) {
    // await SiteImage.destroy({});
    var photo = await SiteImage.findOne().where({
      id: inputs.id
    });
    if (!photo || _.isEmpty(photo)) {
      throw "notFound";
    }
    this.res.type(photo.imageUploadMime);

    var Pipeing = await sails.startDownload(photo.imageUploadFd);

    return Pipeing; // All done.
  }
};
