module.exports = {
  friendlyName: "Conversion value",

  description: "",

  inputs: {
    set: { type: "boolean", required: false },
    get: { type: "boolean", required: false },
    value: { type: "number", required: false }
  },

  exits: {},

  fn: async function(inputs) {
    if (inputs.set && inputs.value) {
      await Site.destroy({ conversionRate: inputs.value });
      var rate = await Site.create({
        conversionRate: inputs.value
      }).fetch();
      return rate.conversionRate;
    }
    if (inputs.get) {
      var rate = await Site.findOne({
        conversionRate: inputs.value
      });
      return rate.conversionRate;
    }
    // All done.
    return;
  }
};
