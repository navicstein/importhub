module.exports = {
  friendlyName: "Update image link or info",

  description: "",

  inputs: {
    url: {
      type: "string",
      required: true
    },
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var img = await SiteImage.update({ id: inputs.id }).set({
      url: inputs.url
    });

    // All done.
    return img;
  }
};
