module.exports = {
  friendlyName: "Delete image",

  description: "",

  inputs: {
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var image = await SiteImage.findOne({ id: inputs.id });
    try {
      await sails.rm(image.imageUploadFd);
      await SiteImage.destroy({ id: image.id });
    } catch (e) {
      sails.log.error(e);
    }
    // All done.
    return;
  }
};
