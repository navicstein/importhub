module.exports = {
  friendlyName: "Contact us",

  description: "",

  inputs: {
    feedback: { type: "string", required: true },
    emailAddress: { type: "string", required: true },
    fullName: { type: "string", required: true }
  },

  exits: {},

  fn: async function(inputs) {
    await sails.helpers.sendEmail.with({
      to: "navicsteinrotciv@gmail.com",
      subject: "New Building plan requested by " + inputs.fullName,
      content: inputs.feedback
    });
    // All done.
    return;
  }
};
