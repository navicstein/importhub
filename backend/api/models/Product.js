/**
 * Products.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: "string",
      required: true,
      unique: true
    },

    slug: {
      type: "string",
      required: true,
      unique: true
    },

    description: {
      type: "string",
      required: true,
      description: "the description of this product",
      example:
        "Made from 100% organic cotton, this classic red men's polo has a slim fit and signature logo embroidered on the left of the chest. Machine wash cold; imported."
    },

    // category would be dynamic
    moq: {
      type: "number",
      required: false,
      description: "Minimum Order Quantiy",
      extendedDescription:
        "This contains the Minimum order quantity a product can have at a particular time",
      example: "^([0, 10])+"
    },

    tags: {
      required: false,
      type: "string"
    },

    stockQuantity: {
      required: false,
      type: "number"
    },

    subCategory: {
      type: "string",
      description: "the sub category for a product",
      example: "laptop/desktops, phones/tablet"
    },

    price: {
      type: "number",
      required: true
    },

    marketPrice: {
      type: "number",
      required: false
    },

    requiresShipping: {
      type: "boolean",
      required: false,
      defaultsTo: true
    },

    rating: {
      type: "string"
    },

    availablity: {
      type: "boolean"
    },

    brand: {
      type: "string",
      example: "product’s brand name"
    },

    color: {
      type: "json",
      example: "product’s colors",
      example: "RED/PINK/BLUE"
    },

    condition: {
      type: "string",
      description: "The condition of the product at time of sale",
      example: "New, Refublished, Used",
      defaultsTo: "New"
    },

    arrivalDate: {
      type: "string",
      defaultsTo: new Date().toISOString().substr(0, 10)
    },

    specifications: {
      type: "json",
      description: "The specs of this product",
      example: "not yet implemented"
    },

    sku: {
      type: "string",
      description: "The stock keeping unit of a product",
      unique: true,
      required: true
    },

    sizes: {
      type: "json",
      example: "['XS', 'LG', 'XL']",
      extendedDescription:
        "Use the size [size] attribute to describe the standardised size of your product. When you use this attribute, your ad can appear in results that are filtered by size. The size that you submit will also affect how your product variants are shown."
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    returnPolicy: {
      type: "string",
      required: false,
      defaultsTo: "No return policy ensured",
      description: "The return policy a product can have",
      example: "^[(60 days money return), (...)]+"
    },

    shipping: {
      type: "json",
      required: false,
      defaultsTo: {
        weight: "2lbs",
        dimensions: {
          width: 10,
          height: 10,
          depth: 1
        }
      }
    },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    seo: {
      type: "json",
      required: false,
      defaultsTo: {
        metaTitle: "",
        metaKeywords: "",
        metaDescription: ""
      }
    },

    isActive: {
      type: "boolean",
      required: false,
      defaultsTo: false,
      description: "specifies if this product is active | public | or not ..."
    },

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    photos: {
      collection: "productphotos",
      via: "product"
    },

    purchases: {
      collection: "purchases",
      via: "product"
    },

    reviews: {
      model: "review"
    },

    vendor: {
      model: "user",
      description: "The user vendor that has this product[s]"
    },
    category: {
      // collection: "category"
      required: true,
      type: "string",
      description: "the main category for a product",
      example: "Computers and assesories"
    }
  }
};
