/**
 * Purchases.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    trackingNumber: {
      required: true,
      type: "string",
      description: "the tracking number of the product"
    },

    country: {
      type: "string",
      required: true
    },
    state: {
      type: "string",
      required: true
    },
    city: {
      type: "string",
      required: true
    },
    phoneNumber: {
      type: "string",
      required: true
    },
    fullName: {
      type: "string",
      required: true
    },
    address: {
      type: "string",
      required: true
    },
    emailAddress: {
      type: "string",
      required: true
    },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    products: {
      type: "json",
      required: true
    },

    user: {
      model: "user"
    }
  }
};
